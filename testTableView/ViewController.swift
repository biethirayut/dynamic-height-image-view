import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

  @IBOutlet weak var tableView: UITableView!

    var images: [String: UIImage] = [:]
    let urls = [
        "https://www.krungsri.com/bank/getmedia/86f3a971-59ea-4388-886e-d0d4e7272e81/Og-Krungsri-auto.jpg.aspx",
        "https://pbs.twimg.com/profile_images/1044862596141191169/UmuIzCkw_400x400.jpg",
        "https://news.pdamobiz.com/wp-content/uploads/2017/08/Pairote-Cheunkrut-Head-of-Krungsri-Auto-Group.jpg",
        "https://vespa.co.th/wp-content/uploads/2018/08/AD-WEB-AUG-18_1200x800_2-04.jpg",
        "https://ananmoney.com/wp-content/uploads/2018/11/Krungsriauto-Prompt-Start-3.png",
        "https://www.krungsri.com/bank/getmedia/86f3a971-59ea-4388-886e-d0d4e7272e81/Og-Krungsri-auto.jpg.aspx",
        "https://pbs.twimg.com/profile_images/1044862596141191169/UmuIzCkw_400x400.jpg",
        "https://news.pdamobiz.com/wp-content/uploads/2017/08/Pairote-Cheunkrut-Head-of-Krungsri-Auto-Group.jpg",
        "https://vespa.co.th/wp-content/uploads/2018/08/AD-WEB-AUG-18_1200x800_2-04.jpg",
        "https://ananmoney.com/wp-content/uploads/2018/11/Krungsriauto-Prompt-Start-3.png"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension

        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return urls.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
            as! TableViewCell

        guard let image = images[urls[indexPath.row]] else {
            DispatchQueue.global().async { [weak self] in
                guard let self = self else { return }
                if let data = try? Data(contentsOf: URL(string: self.urls[indexPath.row])!) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            self.images[self.urls[indexPath.row]] = image
                            tableView.reloadRows(at: [indexPath], with: .automatic)
                        }
                    }
                }
            }
            return cell
        }
        cell.bannerImageView.image = image
        let ratio = image.size.width / image.size.height
        let newHeight = cell.bannerImageView.frame.width / ratio
        cell.imageHeightConstraint.constant = newHeight
        return cell
    }
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

