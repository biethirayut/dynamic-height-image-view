import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
}
